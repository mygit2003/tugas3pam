package com.example.tugas3

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class Activity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_2)
        val bundle : Bundle? = intent.extras

        val outNim: TextView = findViewById(R.id.otvNim)
        val outNama: TextView = findViewById(R.id.otvNama)

        outNim.text = bundle?.getString("nim")
        outNama.text = bundle?.getString("nama")


    }
}
