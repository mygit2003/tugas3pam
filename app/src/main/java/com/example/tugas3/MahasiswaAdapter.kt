import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.view.menu.MenuView.ItemView
import androidx.recyclerview.widget.RecyclerView
import com.example.tugas3.Activity2
import com.example.tugas3.Mahasiswa
import com.example.tugas3.R

class MahasiswaAdapter(
    private val context: Context,
    private val data: ArrayList<Mahasiswa>
) : RecyclerView.Adapter<MahasiswaAdapter.MahasiswaViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MahasiswaAdapter.MahasiswaViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.row, parent, false)
        return MahasiswaViewHolder(view)
    }

    override fun onBindViewHolder(holder: MahasiswaAdapter.MahasiswaViewHolder, position: Int) {
        val mhs: Mahasiswa = data.get(position)
        val currentItem = data[position]
        holder.itemView.setOnClickListener {
            val intent = Intent(context, Activity2::class.java)
            intent.putExtra("nim", currentItem.nim)
            intent.putExtra("nama", currentItem.nama)
            context.startActivity(intent)
        }
        holder.tvNim.text = mhs.nim
        holder.tvNama.text = mhs.nama
    }

    override fun getItemCount(): Int {
        return  data.size
    }

    inner class MahasiswaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvNim: TextView = itemView.findViewById<TextView>(R.id.tvNim)
        val tvNama: TextView = itemView.findViewById<TextView>(R.id.tvNama)
    }
}