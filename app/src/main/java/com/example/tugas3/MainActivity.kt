package com.example.tugas3

import MahasiswaAdapter
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rv1:RecyclerView = findViewById(R.id.rv1)
        val etNim: EditText = findViewById(R.id.etNim)
        val etNama: EditText = findViewById(R.id.editTextTextPersonName2)
        val btSimpan: Button = findViewById(R.id.bt1)
        val data: ArrayList<Mahasiswa> = ArrayList()
        data.addAll(getData())
        val adapter = MahasiswaAdapter(this, data)
        rv1.adapter = adapter
        rv1.layoutManager = LinearLayoutManager(this)

        btSimpan.setOnClickListener {
            val nim = etNim.text.toString()
            val nama = etNama.text.toString()

            if (nim.isNotEmpty() && nama.isNotEmpty()) {
                val mhs = Mahasiswa()
                mhs.nim = nim
                mhs.nama = nama
                data.add(mhs)
                adapter.notifyDataSetChanged()
                etNim.text.clear()
                etNama.text.clear()
            } else {
                Toast.makeText(this, "SALAH SATU DATA TIDAK BOLEH KOSONG", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun getData(): ArrayList<Mahasiswa> {
        val nim = resources.getStringArray(R.array.nim)
        val nama = resources.getStringArray(R.array.nama)
        val data = ArrayList<Mahasiswa>()
        for (i in nim.indices) {
            val mhs = Mahasiswa()
            mhs.nim = nim[i]
            mhs.nama = nama[i]
            data.add(mhs)
        }
        return data
    }
}
